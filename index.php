<?php
include_once ('include/header.php');
$arr = [
    [
        "id"    => 500,
        "p_id"  => 0,
        "title" => "Bangladesh"
    ],
    [
        "id"    => 510,
        "p_id"  => 500,
        "title" => "Dhaka"
    ],
    [
        "id"    => 511,
        "p_id"  => 510,
        "title" => "Uttara"
    ],
    [
        "id"    => 512,
        "p_id"  => 510,
        "title" => "Khailgaon"
    ],
    [
        "id"    => 513,
        "p_id"  => 500,
        "title" => "NoyaKhali"
    ],
    [
        "id"    => 514,
        "p_id"  => 513,
        "title" => "Maijdi"
    ],
    [
        "id"    => 515,
        "p_id"  => 0,
        "title" => "Canada"
    ],
    [
        "id"    => 516,
        "p_id"  => 515,
        "title" => "Toronto"
    ],

];

$arr_size = sizeof($arr);

echo '<div class="container m-auto">';
echo '<div class="col-md-8">';
echo '<ul>';
for ($i=0; $i<$arr_size; $i++){
    if ($arr[$i]['p_id'] == 0){
        echo '<li>';
        echo $arr[$i]['title'];
        for ($j=0; $j<$arr_size; $j++){
            if ($arr[$j]['p_id'] == $arr[$i]['id']){
                echo '<ul>';
                echo $arr[$j]['title'];
                for ($k=0; $k<$arr_size; $k++){
                    if ($arr[$k]['p_id'] == $arr[$j]['id']){
                        echo '<ul>';
                        echo $arr[$k]['title'];
                        echo '</ul>';
                    }
                }
                echo '</ul>';
            }
        }
        echo '</li>';
    }
}
echo '</ul>';
echo '</div>';
echo '</div>';
include_once ('include/footer.php');